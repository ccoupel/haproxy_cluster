#!/bin/bash
TAGS:
 haproxy: deploy haproxy
 pacemaker: deploy pacemaker cluster

ansible-playbook -i ~/ha-proxy.ansible/inventory -i <federation s3 inventory> haproxy.yml 
