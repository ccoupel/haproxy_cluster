playbook to run from the Supervisor

update the inventory file with Haproxy nodes
check that the ansible_user can connect to the haproxy nodes with ssh key


cd /src/scality/s3/s3config/federation
./ansible-playbook -i s3/s3config/inventory -i /root/haproxy-playbook/inventory /root/haproxy-playbook/haproxy.yml

for SSL:
the PEM file must contain, in this order:
- private key PEM
- public key PEM
- intermediate PEM
- root PEM
